SELECT nhan_vien.*, SUM(diem_thi.diem) as tong_diem
FROM nhan_vien JOIN diem_thi ON nhan_vien.id_nhan_vien = diem_thi.id_nhan_vien
WHERE nhan_vien.DEV like 'Dev2' AND diem_thi.ngay_thi = '2021-09-13'
GROUP BY nhan_vien.id_nhan_vien
ORDER BY tong_diem DESC;