let list_nhan_vien = [
    {
        id_nhan_vien: 1,
        name: 'Nguyen Van A',
        birthday: '23/12/1995',
        tong_diem: 50
    },
    {
        id_nhan_vien: 2,
        name: 'Le Van B',
        birthday: '23/12/199',
        tong_diem: 50
    },
    {
        id_nhan_vien: 2,
        name: 'Le Van C',
        birthday: '23/12/1997',
        tong_diem: 50
    }
];

// Chi validate format mm/dd/yyyy, chua check chi tiet ngay thang nam co hop le khong
const isValidDate = (date) => {
    return /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/.test(date);
}

let nhan_vien_hop_le = list_nhan_vien.filter(nhan_vien => isValidDate(nhan_vien.birthday));

alert(JSON.stringify(nhan_vien_hop_le));
