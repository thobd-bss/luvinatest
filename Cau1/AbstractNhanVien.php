<?php
Abstract Class AbstractNhanVien {
    protected $idNhanVien;

    protected $name;

    protected $birthday;

    protected $dev;

    protected $diemThi;

    public function __construct($idNhanVien, $name, $birthday, $dev) {
        $this->idNhanVien = $idNhanVien;
        $this->name = $name;
        $this->birthday = $birthday;
        $this->dev = $dev;
        $this->diemThi = [];
    }

    public function setIdNhanVien($idNhanVien)
    {
        $this->idNhanVien = $idNhanVien;
    }

    public function getIdNhanVien()
    {
        return $this->idNhanVien;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setDev($dev)
    {
        $this->dev = $dev;
    }

    public function getDev()
    {
        return $this->dev;
    }

    public function addDiemThi($ngayThi, $diemThi)
    {
        if (isset($this->diemThi[$ngayThi])) {
            $this->diemThi[$ngayThi]->setDiem($ngayThi);
        } else {
            $this->diemThi[$ngayThi] = new DiemThi($diemThi);
        }
    }

    public function getTongDiem()
    {
        $tongDiem = 0;
        foreach ($this->diemThi as $diemThi) {
            $tongDiem += $diemThi->getTongDiem();
        }
        return $tongDiem;
    }

    public function getRewardValue()
    {
        if ($this->getTongDiem() >= 150 ) return 15000000;
        if ($this->getTongDiem() >= 120 ) return 10000000;
        if ($this->getTongDiem() >= 100 ) return 5000000;
        return 0;
    }
}