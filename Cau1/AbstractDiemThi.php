<?php
Abstract Class AbstractDiemThi {
    const MA_DIEM_TU_VUNG = 1;

    const MA_DIEM_NGU_PHAP = 2;

    const MA_DIEM_DOC_HIEU = 3;

    const MA_DIEM_NGHE_HIEU = 4;

    protected $diemTuVung;

    protected $diemNguPhap;

    protected $diemDocHieu;

    protected $diemNgheHieu;

    public function __construct($diemThi = []) {
        $this->diemTuVung = $diemThi[self::MA_DIEM_TU_VUNG] ?: 0;
        $this->diemNguPhap = $diemThi[self::MA_DIEM_NGU_PHAP] ?: 0;
        $this->diemDocHieu = $diemThi[self::MA_DIEM_DOC_HIEU] ?: 0;
        $this->diemNgheHieu = $diemThi[self::MA_DIEM_NGHE_HIEU] ?: 0;
    }

    public function setDiem($diemThi = []) {
        $this->diemTuVung = $diemThi[self::MA_DIEM_TU_VUNG] ?: 0;
        $this->diemNguPhap = $diemThi[self::MA_DIEM_NGU_PHAP] ?: 0;
        $this->diemDocHieu = $diemThi[self::MA_DIEM_DOC_HIEU] ?: 0;
        $this->diemNgheHieu = $diemThi[self::MA_DIEM_NGHE_HIEU] ?: 0;
    }

    public function getDiem($maDiem)
    {
        switch ($maDiem) {
            case self::MA_DIEM_TU_VUNG:
                return $this->getDiemTuVung();
            case self::MA_DIEM_NGU_PHAP:
                return $this->getDiemNguPhap();
            case self::MA_DIEM_DOC_HIEU:
                return $this->getDiemDocHieu();
            case self::MA_DIEM_NGHE_HIEU:
                return $this->getDiemNgheHieu();
        }

        return null;
    }

    public function getDiemTuVung()
    {
        return $this->diemTuVung;
    }

    public function setDiemTuVung($diemTuVung)
    {
        $this->diemTuVung = $diemTuVung;
    }

    public function getDiemNguPhap()
    {
        return $this->diemNguPhap;
    }

    public function setDiemNguPhap($diemNguPhap)
    {
        $this->diemNguPhap = $diemNguPhap;
    }

    public function getDiemDocHieu()
    {
        return $this->diemDocHieu;
    }

    public function setDiemDocHieu($diemDocHieu)
    {
        $this->diemDocHieu = $diemDocHieu;
    }

    public function getDiemNgheHieu()
    {
        return $this->diemNgheHieu;
    }

    public function setDiemNgheHieu($diemNgheHieu)
    {
        $this->diemNgheHieu = $diemNgheHieu;
    }

    public function getTongDiem()
    {
        return $this->diemTuVung + $this->diemDocHieu + $this->diemNgheHieu + $this->diemNguPhap;
    }
}